# vue-markdown-editor
> A simple, beautiful Markdown editor powered by **[vue.js](https://https://github.com/vuejs/vue), [ace](https://github.com/ajaxorg/ace/),
[markdown-it](https://github.com/markdown-it/markdown-it).**

## Features
* GitHub Flavored Markdown
* Code highlight in editor
* Read/Write/Preview mode
* Support Emojies, Typographic
* Subscript/Superscript, Footnote
* Inserted Text, Marked Text
* Preview HTML Code
* Table of Content
* Export Markdown File

## Screenshot
![Screenshots](static/img/preview.jpeg)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## License
[MIT](https://github.com/eteplus/menote/blob/master/LICENSE)
